<?php


/** 
 * return array of arrays with keys: 
 *  - self - link to page
 *  - image - link to the minifed image
 *  - name - name of the page
 *  - text - text from textprop property
 *
 */
 class SQFcategory extends SMWResultPrinter {
     public function getIMGSZ($res, $width, $imageprop, $city, $specpage, $textprop) {
        $ret = array();
        while ($row = $res->getNext()) {
            $done = array();
            foreach($row as $k => $resarr) {
                if ($resarr->getPrintRequest()->getLabel() == $imageprop) {
                    while ( ( $dataValue = $resarr->getNextDataValue() ) !== false ) { // Property values
                        if ( $dataValue->getTypeID() == '_wpg' ) {
                            $file = wfFindFile($dataValue->getTitle());
                            if (empty($file)) { continue; }
                            $fout = $file->transform(array('height' => $width, 'width' => $width));
                            $done['image'] = $fout->getUrl();
                        }
                    }
                } else if ( $resarr->getPrintRequest()->getLabel() == $textprop and (! empty($textprop)) ) {
                    while ( $dataValue = $resarr->getNextDataValue() ) {
                        $text = $dataValue->getShortHTMLText();
                        $done['text'] = $text;
                    }
                } else if (empty($done['self']) || empty($done['name'])) {
                    $rt = $resarr->getResultSubject()->getTitle();
                    if (!empty($city)) {
                        $tn = $rt->getText();
                        $q = http_build_query(array('category' => $tn, 
                                                    'city' => $city ));
                        $done['self'] = "/" . $specpage . "?" . $q; 
                    } else {
                        $done['self'] = $rt->getFullUrl();
                    }
                    $done['name'] = $rt->getText();
                }
            }
            if (array_key_exists('image', $done) && array_key_exists('self', $done)) {
                array_push($ret, $done);
            }
        }
        return $ret;
     }

    public function getParamDefinitions( array $definitions ) {
        $params = parent::getParamDefinitions( $definitions );
        $params['width'] = array( 
              'type' => 'integer',
              'default' => 400,
              'message' => 'image widths');
              
        $params['imageproperty'] = array(
               'type' => 'string',
               'default' => 'Title picture',
               'message' => 'property to fetch pictures from');
               
        $params['classes'] = array(
               'type' => 'string',
               'default' => 'quadrat',
               'message' => 'can be "strip" or "quadrat"');
               
        $params['x1366'] = array(
                 'type' => 'integer',
                 'default' => 10,
                 'message' => 'apply class x1366 from');

        $params['x1600'] = array(
                    'type' => 'integer',
                    'default' => 14,
                    'message' => 'apply class x1600 from');

        $params['title'] = array(
                        'type' => 'string',
                        'default' => '',
                        'message' => 'title of the box');

        $params['boxclass'] = array(
                         'type' => 'string',
                         'default' => 'grid',
                         'message' => 'box');
                         
        $params['city'] = array (
                         'type' => 'string',
                         'default' => '',
                         'message' => 'city of category');
                         
        $params['specpage'] = array (
                            'type' => 'string',
                            'default' => 'Special:CategoryGallery',
                            'message' => 'used with "city" parameter');

        $params['textprop'] = array (
            'type' => 'string',
            'default' => '',
            'message' => 'Property name to select additional text');

        $params['textpropclass'] = array (
            'type' => 'string',
            'default' => '',
            'message' => 'html class to text property elements');

        $params['nameclass'] = array (
            'type' => 'string',
            'default' => '',
            'message' => 'html class to name elements');
                          
        return $params;
    }

     public function mapQuad($res, $x13, $x16) {
         $ret = array();
         foreach( array_values($res) as $k => $v) {
             if ($k >= $x13 - 1) {
                 $v['class'] = 'x1366';
             }
    
             if ($k >= $x16 - 1) {
                 $v['class'] = 'x1600';
             }
             array_push($ret, $v);
         }
         
         return $ret;
     }
     
     public function mapRender($res, $title, $boxclass, $textpropclass, $nameclass, $classes) {
         ob_start(); ?>
         <div <?php echo $boxclass ? 'class="' . $boxclass . '"' : ''?> >
             <?php if (!empty($title)): ?>
             <div><?php echo $title ?></div>
             <?php endif ?>

             <ul>
<?php            foreach($res as $v):  ?>
             <li <?php if ( ! empty($classes) ) echo 'class="' . $classes . '"';?> >
                    <a href="<?php echo $v['self'] ?>">
                      <img src="<?php echo $v['image'] ?>"></img>
                      <div <?php if ($nameclass) {echo 'class="' . $nameclass . '"';}?>><?php echo $v['name']?></div>
                    </a>
                 
<?php if ( array_key_exists('text', $v) ): ?>
                     <div <?php if ( ! empty($textpropclass) ) echo 'class="' . $textpropclass . '"'; ?>><?php echo $v['text'] ?></div>
<?php endif ?> 
                 </li>
<?php            endforeach ?>
                 <div style="clear:both"></div>
             </ul>
         </div>
<?php    $ret = ob_get_contents();
         ob_end_clean();
         return $ret;
     }
     
     protected function buildResult( SMWQueryResult $results ) {
         return $this->getResultText( $results, $this->outputMode );
     }
     
     public function getResultText( SMWQueryResult $results, $outputmode ) {
         $width = $this->params['width'];
         $imageprop = $this->params['imageproperty'];
         $city = $this->params['city'];
         $specpage = $this->params['specpage'];
         $textprop = $this->params['textprop'];
         $textpropclass = $this->params['textpropclass'];
         $res = $this->getIMGSZ($results, $width, $imageprop, $city, $specpage, $textprop);
        
         $html = $this->mapRender($res,$this->params['title'], $this->params['boxclass'], $textpropclass, $this->params['nameclass'], $this->params['classes']);
         return array( $html, 'nowiki' => true, 'isHTML' => true );
     }
 }   
